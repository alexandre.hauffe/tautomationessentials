package com.example.tautomationessentials.controller;

import com.example.tautomationessentials.dao.UserRepository;
import com.example.tautomationessentials.model.User;
import com.example.tautomationessentials.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/")
public class UserController {

    private UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @RequestMapping("server")
    public String hello() {
        return "Service for Test Automation Essentials Assignment";
    }

    @GetMapping
    public Iterable<User> findAll(){
        return service.findAll();
    }

    @GetMapping(path = {"{id}"})
    public ResponseEntity <?> findById(@PathVariable long id){
        return service.findById(id);
    }

    @PostMapping
    public User create(@RequestBody User user){
        return service.create(user);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity update(@PathVariable("id") long id,
                                 @RequestBody User user) {
        return service.update(id, user);
    }

    @DeleteMapping(path ={"{id}"})
    public ResponseEntity <?> delete(@PathVariable long id) {
        return service.delete(id);
    }
}
