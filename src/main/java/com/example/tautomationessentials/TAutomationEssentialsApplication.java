package com.example.tautomationessentials;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TAutomationEssentialsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TAutomationEssentialsApplication.class, args);
    }

}
