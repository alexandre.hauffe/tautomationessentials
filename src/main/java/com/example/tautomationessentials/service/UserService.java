package com.example.tautomationessentials.service;

import com.example.tautomationessentials.dao.UserRepository;
import com.example.tautomationessentials.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserService {

    @Autowired
    private final UserRepository repository;

    @Autowired
    public UserService(UserRepository repository){
        this.repository = repository;
    }

    public Iterable<User> findAll(){
        return repository.findAll();
    }

    public ResponseEntity findById(Long id){
            return repository.findById(id)
                    .map(record -> ResponseEntity.ok().body(record))
                    .orElse(ResponseEntity.notFound().build());
    }

    public User create(User user){
        return repository.save(user);
    }

    public ResponseEntity <?> delete(Long id){
        return repository.findById(id)
                .map(record -> {
                    repository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }


    public ResponseEntity update(long id, User user) {
            return repository.findById(id)
                    .map(record -> {
                        record.setName(user.getName());
                        record.setBirthday(user.getBirthday());
                        User userUpdated = repository.save(record);
                        return ResponseEntity.ok().body(userUpdated);
                    }).orElse(ResponseEntity.notFound().build());
    }
}
