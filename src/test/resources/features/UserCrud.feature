Feature: User crud feature

  Scenario: Create user
    Given A user named "George" with id 1 saved
    When the client calls GET "/{id}" with id 1
    Then I can find a user called "George"

  Scenario: Delete user
    Given A user named "Frank" with id 2 saved
    When the client calls DELETE "/{id}" with id 2
    Then I can not find a user called "Frank"

  Scenario: Update user
    Given A user named "Robert" with id 3 saved
    When Update user with id 3 changing name to "Ed"
    Then I can find a user called "Ed"