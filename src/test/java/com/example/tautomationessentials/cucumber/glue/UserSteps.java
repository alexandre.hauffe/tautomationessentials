package com.example.tautomationessentials.cucumber.glue;

import com.example.tautomationessentials.dao.UserRepository;
import com.example.tautomationessentials.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserSteps {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository repository;

    private User user;

    private List<User> users;

    @Before
    public void setUp(){
        user = new User();
        users = new ArrayList<>();
        repository.deleteAll();
    }


    /*
    * @Given methods
    */
    @Given("A user named {string} with id {int} saved")
    public void aUserNamedWithIdSaved(String name, int idrequested) {
        //Arrange
        User newUser = new User(idrequested+0L, name, new Date());

        //Act
        ResponseEntity responseEntity = testRestTemplate.postForEntity("/", newUser, User.class);

        //Assert
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
    }


    /*
    * @When methods
    */
    @When("the client calls GET {string} with id {int}")
    public void theClientCallsGETWithId(String route, int idRequested) {
        //Arrange
        String id = "/"+(idRequested+0L);
        ResponseEntity userRetrievedResponse = testRestTemplate.getForEntity(id, User.class);

        //Act
        this.user = (User) userRetrievedResponse.getBody();

        //Assert
        assertEquals(ResponseEntity.ok().build().getStatusCode(), userRetrievedResponse.getStatusCode());
    }

    @When("the client calls DELETE {string} with id {int}")
    public void theClientCallsDELETEWithId(String arg0, int idRequested) {
        //Arrange
        String id = "/"+(idRequested+0L);

        //Act
        testRestTemplate.delete(id);
    }

    @When("Update user with id {int} changing name to {string}")
    public void updateUserWithIdChangingNameTo(int idRequested, String name) {
        //Arrange
        String id = "/"+(idRequested+0L);
        ResponseEntity userRetrievedResponse = testRestTemplate.getForEntity(id, User.class);
        User userCreated = (User) userRetrievedResponse.getBody();
        userCreated.setName(name);

        //Act
        testRestTemplate.put(id, userCreated);

        //Assert
        assertEquals(ResponseEntity.ok().build().getStatusCode(), userRetrievedResponse.getStatusCode());
    }


    /*
    * @Then methods
    */
    @Then("I can find a user called {string}")
    public void iCanFindAUserCalled(String name) throws JsonProcessingException {
        //Act
        users.addAll(Arrays.asList(objectMapper.readValue(testRestTemplate.getForEntity("/", String.class).getBody(), User[].class)));

        //Assert
        assertEquals(users.get(0).getName(), name);
    }

    @Then("I can not find a user called {string}")
    public void iCanNotFindAUserCalled(String name) throws JsonProcessingException {
        //Act
        users.addAll(Arrays.asList(objectMapper.readValue(testRestTemplate.getForEntity("/", String.class).getBody(), User[].class)));

        //Assert
        assertTrue(users.size() == 0);
    }


}
