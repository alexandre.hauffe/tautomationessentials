package com.example.tautomationessentials;

import com.example.tautomationessentials.model.User;
import org.apache.coyote.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserIntegrationTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private List<User> users;

    @BeforeEach
    void setUp() {
        users = prepareList();
    }

    @Test
    void createUser() {
        //Act
        ResponseEntity responseEntity = testRestTemplate.postForEntity("/", users.get(0), User.class);
        User user = (User) responseEntity.getBody();

        //Assert
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(user);
        assertEquals(users.get(0).getName(), user.getName());
    }

    @Test
    void getAllUsers() {
        //Arrange
        testRestTemplate.postForEntity("/", users.get(1), User.class);
        testRestTemplate.postForEntity("/", users.get(2), User.class);

        //Act
        ResponseEntity userListResponse = testRestTemplate.getForEntity("/", Iterable.class);
        List<User> usersRetrieved = (List<User>) userListResponse.getBody();

        //Assert
        assertNotNull(usersRetrieved);
        assertEquals(this.users.size(), usersRetrieved.size());
    }

    @Test
    void getOneUser() {
        //Arrange
        ResponseEntity userCreatedResponse = testRestTemplate.postForEntity("/", users.get(2), User.class);
        User userCreated = (User) userCreatedResponse.getBody();
        String id = "/"+userCreated.getId();

        //Act
        ResponseEntity userRetrievedResponse = testRestTemplate.getForEntity(id, User.class);
        User userRetrieved = (User) userRetrievedResponse.getBody();

        //Assert
        assertEquals(ResponseEntity.ok().build().getStatusCode(), userRetrievedResponse.getStatusCode());
        assertNotNull(userRetrievedResponse.getBody());
        assertNotNull(userRetrieved);
        assertEquals(userCreated, userRetrieved);
    }

    @Test
    void DeleteUser() {
        //Arrange
        ResponseEntity userCreatedResponse = testRestTemplate.postForEntity("/", users.get(0), User.class);
        User userCreated = (User) userCreatedResponse.getBody();
        String id = "/"+userCreated.getId();

        //Act
        testRestTemplate.delete(id);
        ResponseEntity userRetrievedEntity = testRestTemplate.getForEntity(id, User.class);
        User userRetrieved = (User) userRetrievedEntity.getBody();

        //Assert
        assertEquals(ResponseEntity.notFound().build().getStatusCode(), userRetrievedEntity.getStatusCode());
        assertNull(userRetrievedEntity.getBody());
        assertNull(userRetrieved);
    }

    @Test
    void Update() {
        //Arrange
        ResponseEntity userCreatedResponse = testRestTemplate.postForEntity("/", users.get(2), User.class);
        User userCreated = (User) userCreatedResponse.getBody();
        String id = "/"+userCreated.getId();
        userCreated.setName("Margaret");

        //Act
        testRestTemplate.put(id, userCreated);
        ResponseEntity userRetrievedEntity = testRestTemplate.getForEntity(id, User.class);
        User userUpdated = (User) userRetrievedEntity.getBody();

        //Assert
        assertEquals(ResponseEntity.ok().build().getStatusCode(), userRetrievedEntity.getStatusCode());
        assertNotNull(userRetrievedEntity.getBody());
        assertNotNull(userUpdated);
        assertNotEquals(users.get(2), userUpdated);
        assertNotEquals(users.get(2).getName(), userUpdated.getName());
    }


    private List<User> prepareList(){
        List<User> list= new ArrayList<>();
        list.add(new User("user1", new Date()));
        list.add(new User("user2", new Date()));
        list.add(new User("user3", new Date()));
        return list;
    }

}
