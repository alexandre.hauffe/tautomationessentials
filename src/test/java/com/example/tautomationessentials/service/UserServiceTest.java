package com.example.tautomationessentials.service;

import com.example.tautomationessentials.controller.UserController;
import com.example.tautomationessentials.dao.UserRepository;
import com.example.tautomationessentials.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private Iterable<User> userIterator;

    @Mock
    private UserRepository repository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void findAll() {
        //Arrange
        userIterator = prepareList();
        Mockito.when(repository.findAll()).thenReturn(userIterator);

        //Act
        Iterable<User> users = this.userService.findAll();

        //Assert
        assertNotNull(users);
        assertEquals(Boolean.TRUE, users.iterator().hasNext());
    }

    @Test
    void findById() {
        //Arrange
        User userMock = prepareUser();
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(userMock));

        //Act
        ResponseEntity responseEntity = this.userService.findById(1L);
        User user = (User) responseEntity.getBody();

        //Assert
        assertNotNull(responseEntity);
        assertTrue(responseEntity.hasBody());
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertNotNull(user);
        assertEquals(1L, user.getId());
    }

    @Test
    void update(){
        //Arrange
        User userMock = prepareUser();
        User userUpdate = userMock;
        userUpdate.setName("Margaret");
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(userMock));
        Mockito.when(repository.save(userUpdate)).thenReturn(userUpdate);

        //Act
        ResponseEntity responseEntity = this.userService.update(1L, userUpdate);
        User userResponse = (User) responseEntity.getBody();

        //Assert
        assertNotNull(responseEntity);
        assertTrue(responseEntity.hasBody());
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertNotNull(userResponse);
        assertEquals(1L, userResponse.getId());
        assertEquals("Margaret", userResponse.getName());
    }

    @Test
    void create() {
        //Arrange
        User userMock = prepareUser();
        Mockito.when(repository.save(userMock))
                .thenReturn(userMock);

        //Act
        User user = userService.create(userMock);

        //Assert
        assertNotNull(user);
        assertEquals(userMock, user);
    }

    private List<User> prepareList(){
        List<User> list= new ArrayList<>();
        list.add(new User(1L, "user1", new Date()));
        list.add(new User(2L, "user2", new Date()));
        list.add(new User(3L, "user3", new Date()));
        return list;
    }

    private User prepareUser(){
        return new User(1L, "user1", new Date());
    }
}