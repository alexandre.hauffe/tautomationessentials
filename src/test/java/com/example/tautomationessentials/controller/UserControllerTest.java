package com.example.tautomationessentials.controller;

import com.example.tautomationessentials.model.User;
import com.example.tautomationessentials.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private Iterable<User> userIterator;

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void hello() {
        //Act
        String hello = this.userController.hello();

        //Assert
        assertEquals(hello, "Service for Test Automation Essentials Assignment");
    }

    @Test
    void findAll() {
        //Arrange
        userIterator = prepareList();
        Mockito.when(userService.findAll()).thenReturn(userIterator);

        //Act
        Iterable<User> users = this.userController.findAll();

        //Assert
        assertNotNull(users);
        assertEquals(Boolean.TRUE, users.iterator().hasNext());

    }

    @Test
    void findAllEmpty() {
        //Arrange
        userIterator = new ArrayList<>();
        Mockito.when(userService.findAll()).thenReturn(userIterator);

        //Act
        Iterable<User> users = this.userController.findAll();

        //Assert
        assertNotNull(users);
        assertEquals(Boolean.FALSE, users.iterator().hasNext());
    }

    @Test
    void findById() {
        //Arrange
        userIterator = prepareUser();
        Mockito.when(userService.findById(1L)).thenReturn(ResponseEntity.ok().body(userIterator));

        //Act
        ResponseEntity responseEntity = this.userController.findById(1L);
        List<User> users = (List<User>) responseEntity.getBody();

        //Assert
        assertNotNull(responseEntity);
        assertTrue(responseEntity.hasBody());
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertNotNull(users.get(0));
        assertEquals(1L, users.get(0).getId());
    }

    @Test
    void findByIdNotFound() {
        //Arrange
        userIterator = new ArrayList<>();
        Mockito.when(userService.findById(1L)).thenReturn(ResponseEntity.notFound().build());

        //Act
        ResponseEntity responseEntity = this.userController.findById(1L);

        //Assert
        assertNotNull(responseEntity);
        assertEquals(ResponseEntity.notFound().build().getStatusCode(), responseEntity.getStatusCode());
    }

    @Test
    void create() {
        //Arrange
        List<User> userList = prepareUser();
        Mockito.when(userService.create(userList.get(0)))
                .thenReturn(userList.get(0));

        //Act
        User user = userController.create(userList.get(0));

        //Assert
        assertNotNull(user);
        assertEquals(user, userList.get(0));
    }

    @Test
    void createEmpty() {
        //Act
        User user = userController.create(null);

        //Assert
        assertNull(user);
    }

    @Test
    void update(){
        //Arrange
        List<User> userList = prepareUser();
        User userUpdate = userList.get(0);
        userUpdate.setName("Margaret");
        List<User> usersUpdated = new ArrayList<>();
        usersUpdated.add(userUpdate);
        userIterator = usersUpdated;
        Mockito.when(userService.update(userList.get(0).getId(), userUpdate)).thenReturn(ResponseEntity.ok().body(userIterator));

        //Act
        ResponseEntity responseEntity = this.userController.update(userList.get(0).getId(), userUpdate);
        List<User> usersResponse = (List<User>) responseEntity.getBody();

        //Assert
        assertNotNull(responseEntity);
        assertTrue(responseEntity.hasBody());
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertNotNull(usersResponse.get(0));
        assertEquals(userUpdate.getId(), usersResponse.get(0).getId());
        assertEquals("Margaret", usersResponse.get(0).getName());
    }

    @Test
    void delete() {
        //Arrange
        Mockito.when(userService.delete(1L)).thenReturn(ResponseEntity.ok().build());

        //Act
        ResponseEntity responseEntity = this.userController.delete(1L);

        //Assert
        assertNotNull(responseEntity);
        assertEquals(responseEntity.getStatusCode(), ResponseEntity.ok().build().getStatusCode());
    }

    @Test
    void deleteNotFound() {
        //Arrange
        Mockito.when(userService.delete(1L)).thenReturn(ResponseEntity.notFound().build());

        //Act
        ResponseEntity responseEntity = this.userController.delete(1L);

        //Assert
        assertNotNull(responseEntity);
        assertEquals(responseEntity.getStatusCode(), ResponseEntity.notFound().build().getStatusCode());
    }

    private List<User> prepareList(){
        List<User> list= new ArrayList<>();
        list.add(new User(1L, "user1", new Date()));
        list.add(new User(2L, "user2", new Date()));
        list.add(new User(3L, "user3", new Date()));
        return list;
    }

    private List<User> prepareUser(){
        List<User> list= new ArrayList<>();
        list.add(new User(1L, "user1", new Date()));
        return list;
    }
}